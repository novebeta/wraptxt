
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<footer>
    <p class="copyright">
        Copyright &copy; <?php echo date('Y'); ?> Julianto Agung Saputro<br/>
        All Rights Reserved.
    </p>
</footer>

<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="<?= base_url(); ?>assets/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="<?= base_url(); ?>assets/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="<?= base_url(); ?>assets/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="<?= base_url(); ?>assets/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?= base_url(); ?>assets/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?= base_url(); ?>assets/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script src="<?= base_url(); ?>assets/js/main.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="<?= base_url(); ?>assets/js/cors/jquery.xdr-transport.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?= base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?= base_url(); ?>assets/js/sb-admin-2.js"></script>
</body>
</html>