<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title> ANALISIS TEKSTUAL DAN KONTEN LAPORAN TAHUNAN </title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?= base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/jquery.fileupload.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/css/jquery.fileupload-ui.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?= base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        footer {
            position: relative;
            height: 60px;
            width: 100%;
            background-color: #f8f8f8;
        }
        p.copyright {
            position: absolute;
            width: 100%;
            color: #777;
            border-color: #e7e7e7;
            line-height: 20px;
            font-size: 14px;
            text-align: center;
            bottom:0;
        }
    </style>
</head>
<body>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url(); ?>" style="font-size: 28px;font-weight: bold;">ANALISIS TEKSTUAL DAN KONTEN LAPORAN TAHUNAN</a>
        </div>
        <!-- /.navbar-top-links -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="<?= base_url() ?>">Upload File</a>
                    </li>
                    <li>
                        <a href="<?= site_url() . '/readlap/dokumen' ?>">Document Analysis</a>
                    </li>
<!--                    <li>-->
<!--                        <a href="--><?//= site_url() . '/readlap/nerstat' ?><!--"> NER (Name Entity Recognition) Analysis</a>-->
<!--                    </li>-->
                    <li>
                        <a href="<?= site_url() . '/readlap/history' ?>">History</a>
                    </li>
<!--                    <li>-->
<!--                        <a href="--><?//= site_url() . '/readlap/learning' ?><!--">Learning</a>-->
<!--                    </li>-->
                    <li>
                        <a href="<?= site_url(). '/readlap/upload_setting_index' ?>">Upload Keyword List</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>