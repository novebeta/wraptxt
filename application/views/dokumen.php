<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Hasil Analisis Dokumen</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" >
                <div class="panel-heading">
                    <a href="<?php echo base_url() . '/savetxt/' . DIR_PATH; ?>/dokumen_html_ind.xls">
                        Download Hasil Analisis Dokumen – Bahasa Indonesia</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body" style="white-space: nowrap; height: 200px; overflow-x: scroll; overflow-y: scroll;">
                    <div class="row">
						<?= $table_ind; ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" >
                <div class="panel-heading">
                    <a href="<?php echo base_url() . '/savetxt/' . DIR_PATH; ?>/dokumen_html_en.xls">
                        Download Hasil Analisis Dokumen – Bahasa Inggris</a>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body" style="white-space: nowrap; height: 200px; overflow-x: scroll; overflow-y: scroll;">
                    <div class="row">
						<?= $table_en; ?>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<div id="pesan" style="display:none"></div>
<!-- /#page-wrapper -->