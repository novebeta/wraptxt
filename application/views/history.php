<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">History</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Daftar File Text
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
					<?= $table; ?>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<div id="pesan" style="display:none"></div>
<!-- /#page-wrapper -->
<script>
    window.data_ = <?=json_encode( $data_ );?>;

    function getdetail(ab) {
        window.statistik = window.data_[ab.id].statistik;
        // console.log(window.kalimat);
        var table = $("<table></table>");
        for (var key in window.statistik) {
            table.append("<tr><td>" + key + "</td><td>" + window.statistik[key] + "</td></tr>");
        }
        ;
        $("#pesan").html("");
        $("#pesan").append(table);
        // window.open("data:application/vnd.ms-excel," + encodeURIComponent(
        // 		$("#pesan").html()
        // ));
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';
        a.href = data_type + ', ' + encodeURIComponent($("#pesan").html());
        a.download = ab.download;
        a.click();
        // e.preventDefault();
        return (a);
        //saveJSON(window.statistik,"kata.txt");
    }
</script>