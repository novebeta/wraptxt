<?php if ( ! defined( 'BASEPATH' ) ) {
	exit( 'No direct script access allowed' );
}
class Readlap extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *        http://example.com/index.php/welcome
	 *    - or -
	 *        http://example.com/index.php/welcome/index
	 *    - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $path = array();
	public function __construct() {
		parent::__construct();
//		$this->load->helper( 'url' );
		$this->load->helper( 'form' );
		$this->load->library( 'javascript' );
		$this->load->model( 'readtext' );
	}
	public function index() {
//		$id_session   = $this->session->session_id;
		$id_session   = DIR_PATH;
		$templatePath = './savetxt/' . $id_session . '/';
		if ( is_dir( $templatePath ) ) {
			$files = scandir( $templatePath );
			foreach ( $files as $file ) {
				if ( is_file( $templatePath . '/' . $file ) ) {
					unlink( $templatePath . '/' . $file );
				}
			}
		}
		$this->load->view( 'templates/header' );
		$this->load->view( 'upload' );
		$this->load->view( 'templates/footer' );
		/*
		$this->session->unset_userdata('table_result');
		$data['library_src'] = $this->jquery->script();
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/jquery-ui.min.js' );
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/jquery.form.js' );
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/jquery.serializeObject.min.js' );
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/pdf.js' );
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/require.js' );
		$data['library_src'] .= $this->jquery->script( base_url() . 'public/js/pdf.worker.js' );
		$data['script_head'] = $this->jquery->_compile();
		$data['judul']       = $this->config->item( 'aplikasiname' );
		$this->load->view( 'upload', $data );*/
	}
	public function upload_setting_index() {
//		$id_session   = $this->session->session_id;
		$id_session   = DIR_PATH;
		$templatePath = './savetxt/' . $id_session . '/';
		if ( is_dir( $templatePath ) ) {
			$files = scandir( $templatePath );
			foreach ( $files as $file ) {
				if ( is_file( $templatePath . '/' . $file ) ) {
					unlink( $templatePath . '/' . $file );
				}
			}
		}
		$this->load->view( 'templates/header' );
		$this->load->view( 'upload_setting' );
		$this->load->view( 'templates/footer' );
	}


//	public function testreadpdf(){
//		include APPPATH . "/third_party/vendor/autoload.php";
//		$parser = new \Smalot\PdfParser\Parser();
////		$pdf    = $parser->parseFile( $this->filename );
//
//		$pdf = $parser->parseFile( 'slip_gaji.pdf' );
//		$text = $pdf->getText();
//		echo $text;
//		$this->pages = $pdf->getPages();
//		foreach ( $this->pages as $page ) {
//			echo $page->getText();
//		}
//		var_dump($pdf);
//	}
//
//	public function oldindex() {
//		echo "Test Membaca PDF ke Text";
//		$this->load->helper( 'pdf2text' );
//		$filename = "./pdf/lap2.pdf";
//		$text     = pdf2text( $filename );
//		echo $text;
//	}
//	function testword() {
//		$this->readtext->filename = "./pdf/astra.docx";
//		$this->readtext->readWord();
//		$this->readtext->filter();
//		echo $this->readtext->text_len;
//	}
//	function testsi() {
//		$this->readtext->initsilabi();
//		echo $this->readtext->countsilabiword( 'Beberapa orang yang' );
//	}
//	function countword() {
//		$this->readtext->initsilabi();
//		echo $this->readtext->countwordsilabi( 'Beberapa orang yang' );
//	}
//	function testlang() {
//		//$this->readtext->loadlang();
//		$teks = "Sebelumnya, Ketua Dewan Pembina GNPF MUI Habib Rizieq Syihab menyebut akan menggelar doa bersama pada 11 Februari 2017. Menurut Rizieq, aksi tersebut dimaksudkan untuk keamanan pilkada di Jakarta.";
//		echo $teks;
//		for ( $i = 0; $i < 4000; $i ++ ) {
//			echo "<br>$i -" . $this->readtext->ceklang( $teks );
//		}
//	}
	function gettextfrompdf() {
		$berkas                   = json_decode( $this->input->post( 'isian' ) );
		$this->readtext->gofilter = ( isset( $berkas->filter ) ? true : false );
		$this->readtext->text     = $this->input->post( "isi" );
		$this->readtext->filename = $this->input->post( "filename" );
		$this->readtext->gettextpdf();
		$this->readtext->statistik();
	}
	public function dokumen() {
//		echo DIR_PATH;
//		$path_upload = realpath(BASEPATH.'../upload/').'/';
		$this->load->library( 'table' );
		$this->load->view( 'templates/header' );
		$dir       = array( '/savetxt/' );
		$data_push = [];
		$head      = array(
			"Nama File",
			"Kode Emiten",
			"Tahun",
			"Jumlah Kata",
			"Fog Index",
			"Kincaid Index",
			"LoughranBill",
//			"Kata"  #bikin error
		);
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		$map    = directory_map( 'textsetting/' );
		$lstcol = array();
		foreach ( $map as $file ) {
			$namecol = substr( $file, 0, - 4 );
			array_push( $lstcol, $namecol );
		}
		$ditab = uniqid();
		foreach ( $lstcol as $kol ) {
			array_push( $head, $kol );
		}
		array_push( $head, 'Kata Belum Terdaftar' );
		//$this->table->set_template(array('table_open'=>'<table  class="lapku" cellpadding="0" cellspacing="0" border="0">'));
		$tmpl = array(
			'table_open'         => '<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">',
			'heading_row_start'  => '<tr>',
			'heading_row_end'    => '</tr>',
			'heading_cell_start' => '<th>',
			'heading_cell_end'   => '</th>',
			'row_start'          => '<tr class="rw">',
			'row_end'            => '</tr>',
			'cell_start'         => '<td style="word-wrap: break-word;">',
			'cell_end'           => '</td>',
			'row_alt_start'      => '<tr>',
			'row_alt_end'        => '</tr>',
			'cell_alt_start'     => '<td>',
			'cell_alt_end'       => '</td>',
			'table_close'        => '</table>'
		);
		$this->table->set_template( $tmpl );
		$this->table->set_heading( $head );
//		$id_session   = $this->session->session_id;
		$id_session   = DIR_PATH;
		$path         = 'savetxt/' . $id_session . '/';
		$templatePath = realpath( BASEPATH . '/../' . $path );
//		echo $templatePath;
		$data['data_'] = array();
		$ind           = $en = [];
		if ( $templatePath !== false ) {
			$files = scandir( $path );
			foreach ( $files as $file ) {
				if ( is_file( $templatePath . '/' . $file ) ) {
					$filepath = pathinfo( $templatePath . '/' . $file );
					if ( $filepath['extension'] != 'txt' ) {
						continue;
					}
					$this->readtext->filename_short = $filepath['filename'];
					$this->readtext->filename_ext   = $filepath['extension'];
					$this->readtext->filename       = $templatePath . '/' . $file;
					$this->readtext->readTxt();
					$data_stat = $this->readtext->statistik();
					if ( strpos( $this->readtext->filename_short, 'en_' ) === 0 ) {
						$en[] = $data_stat['hasil'];
					}
					if ( strpos( $this->readtext->filename_short, 'ind_' ) === 0 ) {
						$ind[] = $data_stat['hasil'];
					}
//					$this->table->add_row( $data_stat['hasil'] );
//					$data_push[ md5( $this->readtext->filename_short .
//					                 $this->readtext->filename_ext ) ] =
//						[
//							'statistik'  => $data_stat['statistik'],
//							'statistikb' => $data_stat['statistikb'],
//							'kalimat'    => $data_stat['kalimat']
//						];
					$table_stat = "<thead><tr><th>";
					$table_stat .= implode( '</th><th>', [ 'Kata', 'Count' ] );
					$table_stat .= "</th></tr></thead><tbody>";
					foreach ( $data_stat['statistik'] as $key => $row ) {
//						array_map( 'htmlentities', $row );
						$table_stat .= "<tr><td>$key</td><td>$row</td></tr>";
					}
					$table_stat  .= "</tbody>";
					$html_stat   = '<html><body><table>' . $table_stat . '</table></body></html>';
					$table_statb = "<thead><tr><th>";
					$table_statb .= implode( '</th><th>', [ 'Kata', 'Count' ] );
					$table_statb .= "</th></tr></thead><tbody>";
					foreach ( $data_stat['statistikb'] as $key => $row ) {
//						array_map( 'htmlentities', $row );
						$table_statb .= "<tr><td>$key</td><td>$row</td></tr>";
					}
					$table_statb .= "</tbody>";
					$html_statB  = '<html><body><table>' . $table_statb . '</table></body></html>';
					file_put_contents( $templatePath . '/' . 'wcd_' . $this->readtext->filename_short . '.xls', $html_stat );
					file_put_contents( $templatePath . '/' . 'wcdb_' . $this->readtext->filename_short . '.xls', $html_statB );
				}
			}
//			$data['data_'] = $data_push;
		}
		$data['table_en'] = $this->table->generate( $en );
		$this->table->set_heading( $head );
		$data['table_ind'] = $this->table->generate( $ind );
		if ( $templatePath !== false ) {
			$html = '<html><body>' . $data['table_ind'] . '</body></html>';
			file_put_contents( $templatePath . '/' . 'dokumen_html_ind.xls', $html );
			$html = '<html><body>' . $data['table_en'] . '</body></html>';
			file_put_contents( $templatePath . '/' . 'dokumen_html_en.xls', $html );
		}
		$this->load->view( 'dokumen', $data );
		$this->load->view( 'templates/footer' );
	}
	public function history() {
//		$path_upload = realpath(BASEPATH.'../upload/').'/';
		$this->load->library( 'table' );
		$this->load->view( 'templates/header' );
		$dir       = array( '/savetxt/' );
		$data_push = [];
		$head      = array(
			"Nama File",
			'Date Modified',
			"Size",
		);
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		//$this->table->set_template(array('table_open'=>'<table  class="lapku" cellpadding="0" cellspacing="0" border="0">'));
		$tmpl = array(
			'table_open'         => '<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">',
			'heading_row_start'  => '<tr>',
			'heading_row_end'    => '</tr>',
			'heading_cell_start' => '<th>',
			'heading_cell_end'   => '</th>',
			'row_start'          => '<tr class="rw">',
			'row_end'            => '</tr>',
			'cell_start'         => '<td>',
			'cell_end'           => '</td>',
			'row_alt_start'      => '<tr>',
			'row_alt_end'        => '</tr>',
			'cell_alt_start'     => '<td>',
			'cell_alt_end'       => '</td>',
			'table_close'        => '</table>'
		);
		$this->table->set_template( $tmpl );
		$this->table->set_heading( $head );
		$path         = 'savetxt/';
		$templatePath = realpath( BASEPATH . '/../' . $path );
		$files        = scandir( $path );
		foreach ( $files as $file ) {
			if ( is_file( $templatePath . '/' . $file ) ) {
				$filepath = pathinfo( $templatePath . '/' . $file );
				if ( $filepath['extension'] != 'txt' ) {
					continue;
				}
				$time = filemtime( $templatePath . '/' . $file );
				$this->table->add_row( array(
					'<a href="' . base_url() . 'savetxt/' . $file . '">' . $file . '</a>',
					date( "F d Y H:i:s", $time ),
					'<a href="' . base_url() . 'clean/' . $file . '">' . number_format( filesize( './clean/' . $file ) ) . '</a>',
				) );
			}
		}
		$data['table'] = $this->table->generate();
		$this->load->view( 'history', $data );
		$this->load->view( 'templates/footer' );
	}
	public function learning() {
		/*
		 * java -cp stanford-ner/stanford-ner.jar edu.stanford.nlp.process.PTBTokenizer train20.txt > train20.tok
		 * perl -ne 'chomp; print "$_\tO\n"' train20.tok > train20.tsv
		 * java -cp stanford-ner/stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -trainFile train20.tsv -serializeTo ner-model.ser.gz -prop train.prop
		 *
		 */
		$this->load->view( 'templates/header' );
//		$result = array();
//		$fp = fopen(realpath( BASEPATH . '../stanford-ner/KAMUS.csv' ),'r');
//		$row = 0;
//		while (($line = fgetcsv($fp, 0, "\t")) !== FALSE) if ($line) {
//			$result[$line[0]] = $line[1];
//			$row++;
//		}
//		fclose($fp);
//		var_dump($result);
		$this->load->view( 'learning' );
		$this->load->view( 'templates/footer' );
	}
	public function nerstat() {
//		$path_upload = realpath(BASEPATH.'../upload/').'/';
		$this->load->library( 'table' );
		$this->load->view( 'templates/header' );
		$dir          = array( '/txtasli/' );
		$id_session   = DIR_PATH;
		$path         = 'txtasli/' . $id_session . '/';
		$templatePath = realpath( BASEPATH . '/../' . $path );
		$data_push    = [];
		$head         = array(
			"Nama File",
			'PERSON',
			"LOCATION",
			"ORGANIZATION",
		);
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		//$this->table->set_template(array('table_open'=>'<table  class="lapku" cellpadding="0" cellspacing="0" border="0">'));
		$tmpl = array(
			'table_open'         => '<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">',
			'heading_row_start'  => '<tr>',
			'heading_row_end'    => '</tr>',
			'heading_cell_start' => '<th>',
			'heading_cell_end'   => '</th>',
			'row_start'          => '<tr class="rw">',
			'row_end'            => '</tr>',
			'cell_start'         => '<td>',
			'cell_end'           => '</td>',
			'row_alt_start'      => '<tr>',
			'row_alt_end'        => '</tr>',
			'cell_alt_start'     => '<td>',
			'cell_alt_end'       => '</td>',
			'table_close'        => '</table>'
		);
		$this->table->set_template( $tmpl );
		$this->table->set_heading( $head );
		$files = scandir( $templatePath );
//		$pos = new \StanfordTagger\CRFClassifier;
//		$pos->setJarArchive( realpath( BASEPATH . '../stanford-ner/stanford-ner.jar' ) );
//		$pos->setClassifier( realpath( BASEPATH . '../stanford-ner/classifiers/ner-model.ser.gz' ) );
//		$pos->setOutputFormat( \StanfordTagger\CRFClassifier::OUTPUT_FORMAT_SLASH_TAGS );
//		$ind = $en = [];
		$kamus = array();
		$fp    = fopen( realpath( BASEPATH . '../stanford-ner/KAMUS.csv' ), 'r' );
		$row   = 0;
		while ( ( $line = fgetcsv( $fp, 0, "\t" ) ) !== false ) {
			if ( $line ) {
				$kamus[ $line[0] ] = $line[1];
				$row ++;
			}
		}
		fclose( $fp );
		foreach ( $files as $file ) {
			if ( is_file( $templatePath . '/' . $file ) ) {
				$filepath = pathinfo( $templatePath . '/' . $file );
				if ( $filepath['extension'] != 'txt' ) {
					continue;
				}
				$this->readtext->filename_short = $filepath['filename'];
				$this->readtext->filename_ext   = $filepath['extension'];
				$this->readtext->filename       = $templatePath . '/' . $file;
				$this->text                     = file_get_contents( $this->readtext->filename, true );
				$loc                            = '/\/LOCATION/m';
				$per                            = '/\/PERSON/m';
				$org                            = '/\/ORGANIZATION/m';
				$arrper                         = $arrloc = $arrorg = [];
				$xml                            = '';
//				$xml = $pos->tag( $this->text );
				foreach ( $kamus as $key => $val ) {
					$this->text = preg_replace( '/\b' . $key . '\b/', $key . '/' . $val, $this->text );
				}
				$xml = $this->text;
				preg_match_all( $per, $xml, $arrper, PREG_SET_ORDER, 0 );
				preg_match_all( $loc, $xml, $arrloc, PREG_SET_ORDER, 0 );
				preg_match_all( $org, $xml, $arrorg, PREG_SET_ORDER, 0 );
				$id_session = DIR_PATH;
				if ( ! is_dir( './xml/' . $id_session . '/' ) ) {
					mkdir( './xml/' . $id_session . '/', 0777, true );
				}
				$path = 'xml/' . $id_session . '/';
				write_file( $path . $this->readtext->filename_short . '.tag', $xml );
				$ind[] = [
					'<a href="' . base_url() . $path . $this->readtext->filename_short . '.tag' . '">' . $this->readtext->filename_short . '.tag' . '</a>'
					,
					sizeof( $arrper ),
					sizeof( $arrloc ),
					sizeof( $arrorg )
				];
			}
		}
		$data['table_ind'] = $this->table->generate( $ind );
		if ( $templatePath !== false ) {
			$html = '<html><body>' . $data['table_ind'] . '</body></html>';
			file_put_contents( $templatePath . '/' . 'ner_html.xls', $html );
		}
		$this->load->view( 'nerstat', $data );
		$this->load->view( 'templates/footer' );
	}
	public function upload() {
//		echo 'test';
//		die();
		$path_upload = realpath( BASEPATH . '../upload/' ) . '/';
		require( APPPATH . "/third_party/UploadHandler.php" );
		$upload_handler = new UploadHandler( [
			'upload_dir'        => $path_upload,
			'upload_url'        => base_url() . 'upload/',
			'accept_file_types' => '/\.(docx|doc|pdf)$/i'
		], false );
		$response       = $upload_handler->post( true );
//		var_dump($response);
		foreach ( $response['files'] as $file ) {
			$filepath                       = pathinfo( $path_upload . $file->name );
			$this->readtext->filename_short = $filepath['filename'];
			$this->readtext->filename_ext   = $filepath['extension'];
			$this->readtext->filename       = $path_upload . $file->name;
			$this->readtext->readWord();
		}
		$upload_handler->generate_response( $response, true );
//		$response = $upload_handler->response;
//		$files = $response['files'];
//		$file_count = count($files);
//		for ($c = 0; $c < $file_count; $c++) {
//			if (isset($files[$c]->error))
//				continue;
//			$type = $files[$c]->type;
//			$name = $files[$c]->name;
//			$url = $files[$c]->url;
//		}
	}
	public function upload_setting() {
//		echo 'test';
//		die();
		$path_upload = realpath( BASEPATH . '../textsetting/' ) . '/';
		require( APPPATH . "/third_party/UploadHandler.php" );
		$upload_handler = new UploadHandler( [
			'upload_dir'        => $path_upload,
			'upload_url'        => base_url() . 'textsetting/',
			'accept_file_types' => '/\.(txt)$/i'
		], false );
		$response       = $upload_handler->post( true );
//		var_dump($response);
		foreach ( $response['files'] as $file ) {
			$filepath                       = pathinfo( $path_upload . $file->name );
			$this->readtext->filename_short = $filepath['filename'];
			$this->readtext->filename_ext   = $filepath['extension'];
			$this->readtext->filename       = $path_upload . $file->name;
//			$this->readtext->readWord();
		}
		$upload_handler->generate_response( $response, true );
//		$response = $upload_handler->response;
//		$files = $response['files'];
//		$file_count = count($files);
//		for ($c = 0; $c < $file_count; $c++) {
//			if (isset($files[$c]->error))
//				continue;
//			$type = $files[$c]->type;
//			$name = $files[$c]->name;
//			$url = $files[$c]->url;
//		}
	}
	public function uploadner() {
//		echo 'test';
//		die();
		$path_upload = realpath( BASEPATH . '../uploadner/' ) . '/';
		require( APPPATH . "/third_party/UploadHandler.php" );
		$upload_handler = new UploadHandler( [
			'upload_dir'        => $path_upload,
			'upload_url'        => base_url() . 'uploadner/',
			'accept_file_types' => '/\.(txt)$/i'
		], false );
		$response       = $upload_handler->post( true );
//		var_dump($response);
		foreach ( $response['files'] as $file ) {
			$filepath                       = pathinfo( $path_upload . $file->name );
			$this->readtext->filename_short = $filepath['filename'];
			$this->readtext->filename_ext   = $filepath['extension'];
			$this->readtext->filename       = $path_upload . $file->name;
//			$this->readtext->readWord();
			$pos = new \StanfordTagger\CRFClassifier;
			$pos->setJarArchive( realpath( BASEPATH . '../stanford-ner/stanford-ner.jar' ) );
			$pos->setClassifier( realpath( BASEPATH . '../stanford-ner/classifiers/ner-model.ser.gz' ) );
			$pos->setProp( realpath( BASEPATH . '../stanford-ner/classifiers/train.prop' ) );
			$pos->learning( $this->readtext->filename );
		}
		$upload_handler->generate_response( $response, true );
//		$response = $upload_handler->response;
//		$files = $response['files'];
//		$file_count = count($files);
//		for ($c = 0; $c < $file_count; $c++) {
//			if (isset($files[$c]->error))
//				continue;
//			$type = $files[$c]->type;
//			$name = $files[$c]->name;
//			$url = $files[$c]->url;
//		}
	}
	function uploadfile() {
//		$berkas                   = json_decode( $this->input->post( 'input' ) );
//		$this->readtext->gofilter = ( isset( $berkas->filter ) ? true : false );
		$this->readtext->gofilter = false;
//		$filesx                   = explode( "|", $berkas->lst );
		$filesx               = array( 'file1' );
		$cfg['upload_path']   = $this->config->item( 'uploadpdf' );
		$cfg['allowed_types'] = '*';
		$cfg['overwrite']     = true;
		$cfg['remove_spaces'] = true;
		$this->load->library( 'upload', $cfg );
		$this->path = array();
		for ( $i = 0; $i < count( $filesx ); $i ++ ) {
			if ( ! $this->upload->do_upload( $filesx[ $i ] ) ) {
//                echo json_encode(array("hasil" => "1"));
				$err = $this->upload->display_errors( '<div id="eror">', '</div>' );
				echo json_encode( array( "hasil" => $err ) );
			} else {
				$infof = $this->upload->data( $filesx[ $i ] );
				//alert("Upload Berhasil File ".$infof["file_name"]."(".$infof["file_size"]." .Kb)") ;
				//echo "<div>Upload Berhasil File ".$infof["file_name"]."(".$infof["file_size"]." .Kb)\n</div><BR>";
				array_push( $this->path, array( $infof["file_name"], $infof["file_ext"] ) );
//                echo json_encode(array("hasil" => $infof["file_name"]));
			}
		}
		$this->prosesdoc();
//        die();
	}
	function prosesdoc() {
		foreach ( $this->path as $file ) {
			//echo '<br>Proses Pembacaan Dokumen '.$file[0]."-".($this->readtext->gofilter ? "dengan membuang bahasa selain bahasa indonesia.." : "tanpa filter..");
			if ( $file[1] == ".pdf" || $file[1] == ".txt" ) {
				//$this->readtext->filename="./pdf/".$file[0];
				//$this->readtext->readpdf();
				//$this->readtext->getallpages();
				//$this->readtext->statistik();
				echo json_encode( array( "hasil" => "Gunakan Tombol Baca PDF/TXT" ) );
			} else if ( $file[1] == ".docx" ) {
				$this->readtext->filename_short = str_replace( $file[1], '', $file[0] );
				$this->readtext->filename_ext   = $file[1];
				$this->readtext->filename       = "./pdf/" . $file[0];
				$this->readtext->readWord();
				$this->readtext->statistik();
			} else {
				echo "Type File Tidak dikenal....<br>";
			}
		}
	}
	function readpdf() {
		$this->readtext->filename = "./pdf/lap1.pdf";
		$this->readtext->readpdf();
		//$this->readtext->readpage(0);
		$this->readtext->getallpages();
		// echo $this->readtext->text;
	}
	function tes() {
		$this->readpdf();
		$this->readtext->statistik();
		//$this->readtext->text="mempercepat. 2015 pembangunan 332 mempercepat. yang ada di kalimantan serta menumbuhkan kepastian hukum.";
		//$this->readtext->getsentences();
		//$this->readtext->tahunlap($this->readtext->text);
	}
	function semantic() {
		require( APPPATH . "/third_party/opencalais.php" );
		$apikey   = "ucYqGyQicZuXusEBXOs0CfzIpAiJh9l5";
		$oc       = new OpenCalais( $apikey );
		$content  = "
		In order to realize our mission of playing a key role in spurring Indonesia’s long-term economic growth, we at Bank Mandiri make every eﬀort to consistently working for Indonesia. This involves a determination to make Bank Mandiri the bank with the best ﬁnancial and operational performance, and to provide the greatest possible contribution to society and the environment. Our dream is a bank that can give the best to all stakeholders in the form of economic prosperity (proﬁt), social wellbeing (people), and the preservation of the environment (the planet) so that we can contribute to the realization of a sustainable Indonesia.
		";
		$entities = $oc->getEntities( $content );
		foreach ( $entities as $type => $values ) {
			echo "<b>" . $type . "</b>";
			echo "<ul>";
			foreach ( $values as $entity ) {
				echo "<li>" . $entity . "</li>";
			}
			echo "</ul>";
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
