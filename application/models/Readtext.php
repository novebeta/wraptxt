<?php if ( ! defined( 'BASEPATH' ) ) {
	exit( 'No direct script access allowed' );
}
/**
 * Eye View Design CMS module Ajax Model
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   EVD CMS
 * @author    Frederico Carvalho
 * @copyright 2008 Mentes 100Limites
 * @version   0.1
 */
class Readtext extends CI_Model {
	var $filename_short = "";
	var $filename_ext = "";
	var $filename = "";
	var $text = "";
	var $tahun = "";
	var $emiten = "";
	var $oritext = "";
	var $cleantext = "";
	var $baseword = "";
	var $lstcol = array();
	var $text_lid = "";
	var $text_len = "";
	var $textnodogit = "";
	var $minword = 10;
	var $minchar = 3;
	var $maxchar = 10;
	var $sentencelength = 30;
	var $gofilter = false;
	var $contkata = array();
	var $contkatapharse = array();
	var $sentencelist = array();
	var $kata_sudah_hitung = array();
	var $kata_belum_hitung = array();
	var $total_kata_belum_hitung = 0;
	var $dirDokArr = array( './savetxt/', './txtasli/', './upload/', './clean/', './xml/', './uploadner/' );
	function __construct() {
		foreach ( $this->dirDokArr as $dir ) {
			if ( ! is_dir( $dir ) ) {
				mkdir( $dir, 0777, true );
			}
		}
		require( APPPATH . "/third_party/Sentence.php" );
		// Call the Model constructor
		parent::__construct();
		$this->loadlang();
		$this->initsilabi();
		//$this->load->database();
	}
	function readpdf() {
		include APPPATH . "/third_party/vendor/autoload.php";
		$parser = new \Smalot\PdfParser\Parser();
//		$pdf    = $parser->parseFile( $this->filename );
		$pdf = $parser->parseFile( '/home/nove/DATA_D/xampp/htdocs/wraptxt/704416770600_171010.pdf' );
		// Retrieve all pages from the pdf file.
		$this->pages = $pdf->getPages();
//		 Loop over each page to extract text.
		foreach ( $pages as $page ) {
			echo $page->getText();
		}
	}
	function readWordOLD() {
		$this->load->helper( 'word' );
		initword();
		$document = \PhpOffice\PhpWord\IOFactory::load( $this->filename );
//print_r($document);
	}
	function readTxt() {
		$this->ftext     = $this->filename_short . ".txt";
		$this->text      = file_get_contents( $this->filename, true );
		$this->oritext   = $this->text;
		$this->cleantext = $this->oritext;
		$this->cleantext = str_replace( '.', '', $this->cleantext );
	}
	function ExtractData( $obj, $nested = 0 ) {
		$txt = "";
		if ( method_exists( $obj, 'getSections' ) ) {
			foreach ( $obj->getSections() as $section ) {
				$txt .= "\n" . self::ExtractData( $section, $nested + 1 );
			}
		} else if ( method_exists( $obj, 'getElements' ) ) {
			foreach ( $obj->getElements() as $element ) {
				$txt .= "\n" . self::ExtractData( $element, $nested + 1 );
			}
		} else if ( method_exists( $obj, 'getText' ) ) {
			$paragraphStyle = '?';
			if ( method_exists( $obj, 'getTextObject' ) ) {
				$tobj           = $obj->getTextObject();
				$paragraphStyle = $tobj->getParagraphStyle()->getStyleName();
			}
			if ( get_class( $obj ) == "PhpOffice\PhpWord\Element\ListItem" && $obj->getDepth() < 51 ) {
//				$ndx = $obj->getElementIndex();
//				$ns = $obj->getStyle()->getNumStyle();
//				$ni = $obj->getStyle()->getNumId();
				#dd(['getText'=>$obj,  'get_class_methods'=>get_class_methods($obj),'getStyle'=>$obj->getStyle(),'styleMethods'=>get_class_methods($obj->getStyle())]);
				$txt .= "\n" . $obj->getText();// ."($ni:$ns)($paragraphStyle)";
			} else {
				$txt .= "\n";
			}
		}
		return $txt;
	}
	function readWord() {
		$this->load->helper( 'file' );
		if ( $this->readtext->filename_ext == 'doc' || $this->readtext->filename_ext == 'docx' ) {
			$this->load->library( 'readwordppt' );
			$this->readwordppt->filename = $this->filename;
			$this->text                  = $this->readwordppt->convertToText();
		} elseif ( $this->readtext->filename_ext == 'pdf') {
			include APPPATH . "/third_party/vendor/autoload.php";
			$parser     = new \Smalot\PdfParser\Parser();
			$pdf        = $parser->parseFile( $this->filename );
			$text       = $pdf->getText();
			$this->text = $text;
		}
		$this->ftext = $this->filename_short . ".txt";
		$id_session  = DIR_PATH;
		if ( ! is_dir( './txtasli/' . $id_session . '/' ) ) {
			mkdir( './txtasli/' . $id_session . '/', 0777, true );
		}
		$path = 'txtasli/' . $id_session . '/';
//		$templatePath = realpath( BASEPATH . '/../' . $path );
		write_file( $path . $this->ftext, $this->text );
		$this->text = strtolower( $this->text );
//		$readerName = $this->filename_ext == 'doc' ? 'MsDoc' : 'Word2007';
//		$phpword    = \PhpOffice\PhpWord\IOFactory::load( $this->filename, $readerName );
//		$objWriter  = \PhpOffice\PhpWord\IOFactory::createWriter( $phpword, 'HTML' );
//		$objWriter->save( './html/' . $this->filename_short . '.html' );
//		$uploadedText = self::ExtractData($phpword);
//		$sections     = $phpword->getSections();
//		foreach ( $sections as $section ) {
//			$elements = $section->getElements();
//			foreach ( $elements as $element ) {
//				switch ( get_class( $element ) ) {
//					case 'PhpOffice\PhpWord\Element\Text' :
//						$uploadedText .= $element->getText();
//						$uploadedText .= "\n";
//						break;
//					case 'PhpOffice\PhpWord\Element\TextRun' :
//						$textRunElements = $element->getElements();
//						foreach ( $textRunElements as $textRunElement ) {
//							$uploadedText .= $textRunElement->getText();
//							$uploadedText .= "\n";
//						}
//						break;
//					case 'PhpOffice\PhpWord\Element\TextBreak' :
//						$uploadedText .= "\n";
//						break;
//					case 'PhpOffice\PhpWord\Element\Title' :
//						$uploadedText .= $element->getText();
//						$uploadedText .= "\n";
//						break;
//					case 'PhpOffice\PhpWord\Element\Title' :
//						$uploadedText .= $element->getText();
//						$uploadedText .= "\n";
//						break;
//					default :
//						throw new Exception( 'Unknown class type ' . get_class( $element ) );
//				}
//				if ( get_class( $element ) === 'PhpOffice\PhpWord\Element\Text' ) {
//
//				} else if ( get_class( $element ) === 'PhpOffice\PhpWord\Element\TextRun' ) {
//					$textRunElements = $element->getElements();
//					foreach ( $textRunElements as $textRunElement ) {
//						$uploadedText .= $textRunElement->getText();
//						$uploadedText .= ' ';
//					}
//				} else if ( get_class( $element ) === 'PhpOffice\PhpWord\Element\TextBreak' ) {
//					$uploadedText .= ' ';
//				} else {
//					throw new Exception( 'Unknown class type ' . get_class( $element ) );
//				}
//			}
//		}
//		$html          = file_get_contents( './html/' . $this->filename_short . '.html' );
//		$string        = $this->cleantag( $html );
//		$this->text    = $string;
		$this->oritext = $this->text;
		$string        = $this->oritext;
//		$arr                         = explode( PHP_EOL, $string );
		$string = $this->removedigitoneline( $string );
		$string = $this->removeromanoneline( $string );
//		$string = $this->removeminchar( $string );
//		$string = $this->removemaxchar( $string );
//		$string                      = implode( PHP_EOL, $string );
//		require( APPPATH . "/third_party/Sentence.php" );
//		preg_match_all( '/([^\.\!\?]+)/', $string, $cari );
//		$Sentence = new Sentence;
		// Split into array of sentences
//		$sentences = $Sentence->split( $string );
//		$t         = new Trainer();
//		$t->setMaxNgrams( 9000 );
//		$t->learn();
//		$ld = new Language( [ 'en', 'id' ] );
//		$ld->setMaxNgrams( 9000 );
		$en_arr = $id_arr = [];
//		foreach ( $sentences as $row ) {
//			$word_en = $word_id = [];
//			$words   = explode( ' ', $row );
//			foreach ( $words as $word ) {
//			$arr = $ld->detect( $row )->bestResults()->close();
//			reset( $arr );
//			$first_key = key( $arr );
//			if ( $first_key == 'id' ) {
//				$id_arr[] = $row;
//			} else if ( $first_key == 'en' ) {
//				$en_arr[] = $row;
//			}
//			}
//			if ( count( $word_en ) > 0 ) {
//				$en_arr[] = implode( ' ', $word_en );
//			}
//			if ( count( $word_id ) > 0 ) {
//				$id_arr[] = implode( ' ', $word_id );
//			}
//		}
//		write_file( './sentence/' . $this->ftext, implode( "\n", $sentences ) );
		//$this->cleantext=$this->cleanteks($this->text);
//		$string = $this->combinephrase( $string );
		$string = $this->removecomma( $string );
		$string = $this->justwordanddot( $string );
//		$string        = $this->makeonelinebreak( $string );
		$string        = $this->removeblank( $string );
		$this->oritext = $string;
//		if ( count( $en_arr ) > 0 ) {
//			write_file( './savetxt/en_' . $this->ftext, $string );
//		}
//		if ( count( $id_arr ) > 0 ) {
//			write_file( './savetxt/id_' . $this->ftext, $string );
//		}
//		$id_session = $this->session->session_id;
		$id_session = DIR_PATH;
		if ( ! is_dir( './savetxt/' . $id_session . '/' ) ) {
			mkdir( './savetxt/' . $id_session . '/', 0777, true );
		}
		$this->text = $string;
		$this->getsentencelist();
		foreach ( $this->sentencelist as $kalimat ) {
			$lang = $this->ceklang( $kalimat );
//			echo $lang;
			if ( $lang == "ind" ) {
//			$this->text_lid.=" ".$kalimat.".";
				$id_arr[] = $kalimat;
//				echo 'ID : ' . $kalimat . PHP_EOL;
//			$this->sentencecount++;
			}
			if ( $lang == "en" ) {
//				$this->text_len .= " $kalimat.";
				$en_arr[] = $kalimat;
//				echo 'EN : ' . $kalimat . PHP_EOL;
			}
		}
		if ( count( $id_arr ) > 0 ) {
			$string_id = implode( ' ', $id_arr );
			write_file( './savetxt/' . $id_session . '/ind_' . $this->ftext, $string_id );
			write_file( './savetxt/ind_' . $this->ftext, $string_id );
			$this->cleantext = str_replace( '.', '', $string_id );
			write_file( './clean/ind_' . $this->ftext, $this->cleantext );
		}
		if ( count( $en_arr ) > 0 ) {
			$string_en = implode( ' ', $en_arr );
			write_file( './savetxt/' . $id_session . '/en_' . $this->ftext, $string_en );
			write_file( './savetxt/en_' . $this->ftext, $string_en );
			$this->cleantext = str_replace( '.', '', $string_en );
			write_file( './clean/en_' . $this->ftext, $this->cleantext );
		}
//		$this->filesizeindex = number_format( filesize( './savetxt/' . $this->ftext ) ) . "";
//		$this->saveasliteks( $this->oritext );
//		$this->cleantext = $this->oritext;
//		$this->filesizeindex = number_format( filesize( './clean/id_' . $this->ftext ) ) . "";
	}
	function readpage( $page = 0 ) {
		$this->text      = $this->pages[ $page ]->getText();
		$this->oritext   = $this->text;
		$this->cleantext = $this->cleanteks( $this->text );
	}
	function getallpages() {
		$this->text = "";
		foreach ( $this->pages as $page ) {
			$this->text .= $page->getText();
		}
		$this->oritext   = $this->text;
		$this->cleantext = $this->cleanteks( $this->text );
	}
	function gettextpdf() {
		$this->cleantext = $this->cleanteks( $this->text );
	}
	function getkatadasar( $text = "" ) {
		include APPPATH . '/third_party/vendorsastrawi/autoload.php';
		// create stemmer
		// cukup dijalankan sekali saja, biasanya didaftarkan di service container
		$stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
		$stemmer        = $stemmerFactory->createStemmer();
		return $stemmer->stem( $text );
	}
	function initsilabi() {
		/*
		Syllable class reference

	The following is an incomplete list, containing only the most common methods. For a complete documentation of all classes, read the generated PHPDoc.
	public static __construct( $language = 'en', $hyphen = null )

	Create a new Syllable class, with defaults
	public static setCacheDir( $dir )

	Set the directory where compiled language files may be stored. Default to the cache subdirectory of the current directory.
	public static setLanguageDir( $dir )

	Set the directory where language source files can be found. Default to the languages subdirectory of the current directory.
	public setLanguage( $language )

	Set the language whose rules will be used for hyphenation.
	public setHyphen( Mixed $hyphen )

	Set the hyphen text or object to use as a hyphen marker.
	public array splitWord( $word )

	Split a single word on where the hyphenation would go.
	public array splitText( $text )

	Split a text on where the hyphenation would go.
	public string hyphenateWord( $word )

	Hyphenate a single word.
	public string hyphenateText( $text )

	Hyphenate all words in the plain text.
	public string hyphenateHtml( $html )

	Hyphenate all readable text in the HTML, excluding HTML tags and attributes.
	public array histogramText( $text )

	Count the number of syllables in the text and return a map with syllable count as key and number of words for that syllable count as the value.
	public integer countWordsText( $text )

	Count the number of words in the text.
	public integer countPolysyllablesText( $text )

	Count the number of polysyllables in the text.
		*/
		require_once( APPPATH . 'third_party/syllable/classes/autoloader.php' );
		$languages      = array( 'id' => 'Indonesian' );
		$this->syllable = new Syllable( 'id' );
		$this->syllable->getCache()->setPath( APPPATH . 'third_party/syllable/cache' );
		$this->syllable->getSource()->setPath( APPPATH . 'third_party/syllable/languages' );
	}
	function countsilabi( $word = "" ) {
		return count( $this->syllable->splitText( $word ) );
	}
	function countwordsilabi( $teks = "" ) {
		return $this->syllable->countWordsText( $teks );
	}
	function countsilabiword( $teks = "" ) {
		return count( $this->syllable->splitWord( $teks ) ) + 1;
	}
	function init() {
		//if(!$this->gofilter){
		$this->kata_belum_hitung       = [];
		$this->kata_sudah_hitung       = [];
		$this->total_kata_belum_hitung = 0;
		$this->getword( $this->cleantext );
//        $this->getword($this->text);
		$this->getsentences( $this->text );
		//}
//	else{
//		$this->filter();
//	}
		$this->getkomplek();
		//$this->katadasar=$this->getkatadasar($this->cleantext);
		$this->katadasar = $this->cleantext;
//		$this->contkata  = array_count_values( explode( " ", $this->cleantext ) );
		$this->contkata = array_count_values( explode( " ",
			str_replace( "\r\n", " ", $this->cleantext ) ) );
		unset( $this->contkata[' '] );
		foreach ( $this->contkata as $key => $val ) {
			if ( strlen( $key ) == 1 ) {
				unset( $this->contkata[ $key ] );
			}
		}
	}
	function proses() {
		$this->init();
		$this->tahunlap();
		$this->kodeemiten();
		$this->getfogindex();
		$this->getkincaid();
		//load identitas laporan
		$this->gettextsetting();
		$this->savesizeteks();
		$this->getsentencelist();
		$this->hitungkatalain();
	}
	function hitungkatalain() {
		$this->contkatapharse = array_count_values( explode( " ",
			str_replace( "\r\n", " ", $this->katadasar ) ) );
		unset( $this->contkatapharse[' '] );
		foreach ( $this->contkatapharse as $key => $val ) {
			if ( strlen( $key ) == 1 ) {
				unset( $this->contkatapharse[ $key ] );
			}
		}
		$this->kata_belum_hitung       = $this->array_remove_keys( $this->contkatapharse, $this->kata_sudah_hitung );
		$this->total_kata_belum_hitung = array_sum( array_values( $this->kata_belum_hitung ) );
//		$kalimat                       = $this->cleantext;
//		$kalimat                       = str_replace( $this->kata_sudah_hitung, '', $kalimat );
//		$this->total_kata_belum_hitung = 0;
//		while ( count( explode( ' ', $kalimat ) ) > 0 ) {
//			$first = strtok( $kalimat, " " );
//			$first = trim( $first );
//			if ( $first == null ) {
//				continue;
//			}
//			$cari = array();
//			preg_match_all( '/ ' . $first . ' /', $kalimat, $cari );
//			$jml                               = count( $cari[0] );
//			$this->kata_belum_hitung[ $first ] = $jml;
//			$this->total_kata_belum_hitung     += $jml;
//			$kalimat                           = str_replace( $first, '', $kalimat );
//			$kalimat                           = trim( $kalimat );
//			if ( $kalimat == null ) {
//				break;
//			}
//		}
	}
	function array_remove_keys( $array, $keys ) {
		// array_diff_key() expected an associative array.
		$assocKeys = array();
		foreach ( $keys as $key ) {
			$assocKeys[ $key ] = true;
		}
		return array_diff_key( $array, $assocKeys );
	}
	function gettextsetting() {
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		$map             = directory_map( './textsetting/' );
		$this->lstcol    = array();
		$this->katadasar = $this->combinephrase( $this->katadasar );
		foreach ( $map as $file ) {
			$namecol      = substr( $file, 0, - 4 );
			$namecolcount = $namecol . "count";
			array_push( $this->lstcol, $namecol );
			$pattern        = file_get_contents( './textsetting/' . $file );
			$this->$namecol = explode( "\n", $pattern );
			foreach ( $this->$namecol as $key => $row ) {
				if ( trim( $row ) == null ) {
					unset( $this->$namecol[ $key ] );
				}
			}
			$this->kata_sudah_hitung = array_merge( $this->kata_sudah_hitung, $this->$namecol );
			$this->$namecolcount     = $this->sensorword( $this->$namecol );
		}
		$this->kata_sudah_hitung = array_map( 'trim', $this->kata_sudah_hitung );
	}
	function gettextphrase() {
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		$pattern      = read_file( './pharse/setting.txt' );
		$this->phrase = explode( "\n", $pattern );
	}
	function readmytext() {
		$this->load->helper( 'directory' );
		$this->load->helper( 'file' );
		$this->text      = read_file( $this->filename );
		$this->cleantext = $this->cleanteks( $this->text );
	}
	function tahunlap() {
		$re = '/\d{4}/';
		preg_match( $re, $this->filename_short, $matches, PREG_OFFSET_CAPTURE, 0 );
		if ( count( $matches ) > 0 ) {
			$this->tahun = $matches[0][0];
		}
	}
	function kodeemiten() {
		$re = '/^(ind|en)_(\w{4})/';
		preg_match( $re, $this->filename_short, $matches, PREG_OFFSET_CAPTURE, 0 );
		if ( count( $matches ) == 3 ) {
			$this->emiten = $matches[2][0];
		}
	}
	function sensorword( $text = array() ) {
		$jum = 0;
		foreach ( $text as $kata ) {
			$kata = trim( $kata );
			$cari = array();
			preg_match_all( '/\b' . $kata . '\b/', $this->katadasar, $cari );
			if ( count( $cari[0] ) > 0 ) {
				foreach ( $cari[0] as $kol ) {
					if ( ! empty( $kol ) ) {
						$jum ++;
					}
				}
			}
//			$count = str_word_count( $this->katadasar, 0, $kata );
//			if ( $count > 0 ) {
//				$jum ++;
//			}
		}
		return $jum;
	}
	function getword( $string = "" ) {
		$this->wordlist = str_word_count( $string, 2 );
		//print_r($this->wordlist);
		//$this->silabicount=$this->countsilabiword($this->text);
		$this->silabicount = 0;
		foreach ( $this->wordlist as $kata ) {
			$this->silabicount += $this->countsilabiword( $kata );
		}
		// $this->silabicount+=$this->countwordsilabi($kata);
		$this->wordcount = count( $this->wordlist );
	}
	function savesizeteks() {
		//$this->infofile=array();
		$this->load->helper( 'file' );
//	$this->ftext="TEKS-".uniqid().".txt";
//		write_file( './savetxt/' . $this->ftext, $this->cleantext );
		$this->filesizeindex = number_format( filesize( './clean/' . $this->ftext ) ) . "";
	}
	function saveasliteks( $string = "" ) {
		//$this->infofile=array();
		$this->load->helper( 'file' );
		$this->ftext = $this->filename_short . ".txt";//"TEKS-".uniqid().".txt";
		write_file( './savetxt/' . $this->ftext, $string );
		$this->filesizeindex = number_format( filesize( './savetxt/' . $this->ftext ) ) . "";
	}
	function cleanteks( $string = "" ) {
		$string = $this->combinephrase( $string );
		$string = $this->removeminchar( $string );
		//$string=$this->removemaxchar($string);
		$string = $this->cleantag( $string );
		$string = $this->removedigit( $string );
		$string = $this->mustbeA( $string );
		$string = $this->removekatasambung( $string );
		$string = $this->remove1char( $string );
		$string = $this->removeblank( $string );
		return $string;
	}
	function combinephrase( $string = "" ) {
		$this->gettextphrase();
		foreach ( $this->phrase as $pr ) {
			$replace = str_replace( " ", "", $pr );
			$patt    = '/' . trim( $pr ) . '/';
			$string  = preg_replace( $patt, "$replace", $string, - 1 );
		}
		return $string;
	}
	function justwordanddot( $string = "" ) {
		$all_words = [];
		preg_match_all( '/\b[a-z]+\b\.*\s*/m', $string, $all_words );
		return implode( ' ', $all_words[0] );
	}
	function makeonelinebreak( $string = "" ) {
		return preg_replace( '/(\R){2,}/m', "\n", $string );
	}
	function mustbeA( $string = "" ) {
		return preg_replace( '/[^. A-Za-z]/', '', $string );
	}
	function removeminchar( $string = "" ) {
		return preg_replace( "/\b[a-z]{1,$this->minchar}\b/u", '', $string );
	}
	function removemaxchar( $string = "" ) {
		return preg_replace( "/\b[a-z]{1,$this->maxchar}\b/u", '', $string );
	}
	function remove1char( $string = "" ) {
		return preg_replace( "@\\b[a-z]\\b ?@i", "", $string );
	}
	function removeblank( $string = "" ) {
		return preg_replace( '/\s{2,}/m', ' ', $string );
	}
	function removecomma( $string = "" ) {
		return preg_replace( '/\,+/m', ' ', $string );
	}
	function cleantag( $string = "" ) {
		//remove tag <>
		$string = html_entity_decode( $string );
//		if ( is_array( $string ) ) {
//			foreach ( $string as $line ) {
//				$line = html_entity_decode( $line );
//			}
//		}
		return preg_replace( '/<[^>]+>/m', '', $string );
	}
	function removekatasambung( $string = "" ) {
		return preg_replace( '/ dan | ke | di | yang /', '', $string );
	}
	function removedigit( $string = "" ) {
//		return preg_replace( '/(%|$|Rp|\r|\n|\t|[0-9]|[.])/', '', $string );
//		$string = preg_replace( '/^\d+\s*$/', '', $string );
		return preg_replace( '/(%|$|Rp|\d+)/', '', $string );
	}
	function removedigitoneline( $string = "" ) {
		return preg_replace( '/^[\d .,]*\s*$/m', '', $string );
	}
	function removeromanoneline( $string = "" ) {
		return preg_replace( '/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})\s*$/m', '', $string );
	}
	function removedigitdotcomma( $string = "" ) {
		return preg_replace( '/(?<=\d),|([\.-])|([R|r]p)|%|$|\/(?=\d)/', '', $string );
	}
	function getsentences( $string = "" ) {
//		preg_match_all( '/([^\.\!\?]+)/', $string, $cari );
		$Sentence = new Sentence;
		// Split into array of sentences
		$sentences = $Sentence->split( $string );
		// Count the number of sentences
		$count = $Sentence->count( $string );
//		$this->sentencecount = count( $cari[0] );
		$this->sentencecount = $count;
	}
	function getfogindex() {
		if ( $this->sentencecount > 0 && $this->wordcount > 0 ) {
			$this->fogindex = number_format( 0.4 * ( $this->wordcount / $this->sentencecount ) + 100 * ( $this->komplekcount / $this->wordcount ), 2 );
		} else {
			$this->fogindex = 0;
		}
	}
	function getkincaid() {
		if ( $this->sentencecount > 0 && $this->wordcount > 0 ) {
			$this->kincaidindex = number_format( 0.39 * ( $this->wordcount / $this->sentencecount ) + 11.8 * ( $this->silabicount / $this->wordcount ) - 15.59, 2 );
		} else {
			$this->kincaidindex = 0;
		}
	}
	function getkomplek() {
		$imbuhan = array(
			'/^ber(.*)i$/',
			'/^di(.*)an$/',
			'/^ke(.*)i$/',
			'/^ke(.*)an$/',
			'/^me(.*)an$/',
			'/^me(.*)an$/',
			'/^ter(.*)an$/',
			'/^per(.*)an$/',
		);
		$jum     = 0;
		foreach ( $imbuhan as $kata ) {
			$kata = trim( $kata );
			$cari = array();
			preg_match_all( $kata, $this->text, $cari );
			if ( count( $cari[0] ) > 0 ) {
				foreach ( $cari[0] as $kol ) {
					if ( ! empty( $kol ) ) {
						$jum ++;
					}
				}
			}
		}
		$this->komplekcount = $jum;
	}
	function statistik() {
//		$id_session = $this->session->session_id;
		$id_session = DIR_PATH;
		$this->proses();
		$data = array(
			$this->filename_short . '.' . $this->filename_ext,
			$this->emiten,
			$this->tahun,
			'<a href="' . base_url() . '/savetxt/' . $id_session . '/wcd_' . $this->filename_short . '.xls "> ' . $this->wordcount . '</a>',
			$this->fogindex,
			$this->kincaidindex,
			$this->filesizeindex
		);
		foreach ( $this->lstcol as $kol ) {
			$namecolcount = $kol . "count";
			array_push( $data, $this->$namecolcount );
		}
		array_push( $data,
			'<a href="' . base_url() . '/savetxt/' . $id_session . '/wcdb_' . $this->filename_short . '.xls "> ' . $this->total_kata_belum_hitung . '</a>'
		);
//		echo json_encode( array(
//			"hasil"     => "<div class=\"oklap\">" . $this->table->generate() . "</div>",
//			"statistik" => $this->contkata,
//			"kalimat"   => $this->sentencelist
//		) );
		return array(
//			"hasil"     => "<div class=\"oklap\">" . $this->table->generate() . "</div>",
			'hasil'      => $data,
			"statistik"  => $this->contkata,
			"statistikb" => $this->kata_belum_hitung,
			"kalimat"    => $this->sentencelist
		);
	}
	function loadlang() {
		include APPPATH . "third_party/langdetect/classifier.php";
		$this->classifier = new NGramProfiles( APPPATH . "third_party/langdetect/etc/classifiers/full.dat" );
		if ( ! $this->classifier->exists() ) {
			$this->classifier->train( 'en', APPPATH . 'third_party/langdetect/etc/data/english.raw' );
			$this->classifier->train( 'ind', APPPATH . 'third_party/langdetect/etc/data/indonesian.raw' );
			$this->classifier->save();
		} else {
			$this->classifier->load();
		}
	}
	function getwordlist() {
		$this->wordlist = explode( ' ', $this->text );
	}
	function getsentencelist() {
		//remove digit
		$cari = [];
//		$nodigit = preg_replace( '/(%|$|Rp|\r|\n|\t)/', '', $this->text );
		preg_match_all( '/([^\.\!\?]+\.+)/', $this->text, $cari );
		$clearsen = array();
		foreach ( $cari[0] as $sen ) {
//			if ( strlen( $sen ) > $this->sentencelength ) {
//				$jum = explode( ' ', $sen );
//				if ( count( $jum ) >= $this->minword ) {
			array_push( $clearsen, $sen );
//				}
//			}
		}
		$this->sentencelist = $clearsen;
	}
	function ceklang( $text = "" ) {
//klasifikasi bahasa (inggris/indonesia)
		return $this->classifier->predict( $text, 40000 ); // 'en_US'
	}
	function filter() {
		//filter indonesia saja..bhs inggrisnya dibuang
		$this->getsentencelist();
		$this->wordind       = 0;
		$this->worden        = 0;
		$this->sentencecount = 0;
		$this->text_lid      = "";
		foreach ( $this->sentencelist as $kalimat ) {
			$lang = $this->ceklang( $kalimat );
			//if($lang=="id"){
			//$this->text_lid.=" ".$kalimat.".";
			//echo $kalimat;
			//$this->sentencecount++;
			//}
			//if($lang=="en")
			//	$this->text_len.=" $kalimat.";
		}
		$this->text      = $this->text_lid;
		$this->cleantext = $this->cleanteks( $this->text_lid );
		$this->getword( $this->cleantext );
	}
}
