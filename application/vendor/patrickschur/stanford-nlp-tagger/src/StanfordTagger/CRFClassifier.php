<?php
declare( strict_types=1 );
namespace StanfordTagger;
use LanguageDetection\Language;
/**
 * Class CRFClassifier
 *
 * @author Patrick Schur <patrick_schur@outlook.de>
 * @package StanfordTagger
 */
class CRFClassifier extends StanfordTagger {
	/**
	 * @var Language
	 */
	private $lang;
	/**
	 * @var string
	 */
	private $classfier;
	/**
	 * @var string
	 */
	private $prop;
	/**
	 * @return string
	 */
	public function getProp(): string {
		return $this->prop;
	}
	/**
	 * @param string $prop
	 */
	public function setProp( string $prop ) {
		$this->prop = $prop;
	}
	public function __construct() {
		$this->lang = new Language( [ 'de', 'en', 'es', 'zh-Hans', 'zh-Hant' ] );
	}
	/**
	 * @param $str
	 *
	 * @return null|string
	 */
	public function tag( $str ) {
		$str = trim( $str );
		if ( empty( $str ) ) {
			return null;
		}
		$it = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( '.' ) );
		if ( empty( $this->getClassifier() ) ) {
			$lookupTable = [
				'de'      => 'german.conll.hgc_175m_600.crf.ser.gz',
				'en'      => 'english.all.3class.distsim.crf.ser.gz',
				'es'      => 'spanish.ancora.distsim.s512.crf.ser.gz',
				'zh-Hans' => 'chinese.misc.distsim.crf.ser.gz',
				'zh-Hant' => 'chinese.misc.distsim.crf.ser.gz',
			];
			$lang        = $this->lang->detect( $str )->bestResults()->close();
			if ( 1 === count( $lang ) ) {
				$lang = $lookupTable[ array_keys( $lang )[0] ];
			} else {
				$lang = $lookupTable['en'];
			}
			$regex = new \RegexIterator( $it, '/^.+\.crf\.ser\.gz/i', \RecursiveRegexIterator::GET_MATCH );
			foreach ( $regex as $value ) {
				if ( stripos( $value[0], $lang ) !== false ) {
					$this->setClassifier( $value[0] );
					break;
				}
			}
			if ( empty( $this->getClassifier() ) ) {
				throw new \RuntimeException( 'No classifier was found!' );
			}
		}
		if ( empty( $this->getJarArchive() ) ) {
			$regex = new \RegexIterator( $it, '/^.+stanford-ner\.jar$/i', \RecursiveRegexIterator::GET_MATCH );
			foreach ( $regex as $value ) {
				$this->setJarArchive( $value[0] );
				break;
			}
			if ( empty( $this->getJarArchive() ) ) {
				throw new \RuntimeException( 'Could not found any .jar files!' );
			}
		}
		$cmd            = escapeshellcmd(
			$this->getJavaPath()
			. ' -mx' . $this->getMaxMemoryUsage()
			. ' -cp "' . $this->getJarArchive() . PATH_SEPARATOR . '" edu.stanford.nlp.ie.crf.CRFClassifier'
			. ' -loadClassifier ' . $this->getClassifier()
			. ' -textFile ' . $this->getTmpFile( $str )
			. ' -outputFormat ' . $this->getOutputFormat()
		);
		$descriptorspec = [
			0 => [ "pipe", "r" ],
			1 => [ "pipe", "w" ],
			2 => [ "pipe", "r" ]
		];
		$process        = proc_open( $cmd, $descriptorspec, $pipes );
		$output         = null;
		if ( is_resource( $process ) ) {
			fclose( $pipes[0] );
			$output = stream_get_contents( $pipes[1] );
			fclose( $pipes[1] );
			fclose( $pipes[2] );
			proc_close( $process );
		}
		return trim( $output );
	}
	/**
	 * @param $path_file_txt
	 *
	 * @return null|string
	 */
	public function learning( $path_file_txt ) {
		$path_file_tox = substr_replace( $path_file_txt, '.tok', - 4 );
		$path_file_tsv = substr_replace( $path_file_txt, '.tsv', - 4 );
		$str           = trim( $path_file_txt );
		if ( empty( $str ) ) {
			return null;
		}
		$kamus = array();
		$fp    = fopen( realpath( BASEPATH . '../stanford-ner/KAMUS.csv' ), 'r' );
		$row   = 0;
		while ( ( $line = fgetcsv( $fp, 0, "\t" ) ) !== false ) {
			if ( $line ) {
				$kamus[ $line[0] ] = $line[1];
				$row ++;
			}
		}
		fclose( $fp );
		$it = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( '.' ) );
		if ( empty( $this->getClassifier() ) ) {
			$lookupTable = [
				'de'      => 'german.conll.hgc_175m_600.crf.ser.gz',
				'en'      => 'english.all.3class.distsim.crf.ser.gz',
				'es'      => 'spanish.ancora.distsim.s512.crf.ser.gz',
				'zh-Hans' => 'chinese.misc.distsim.crf.ser.gz',
				'zh-Hant' => 'chinese.misc.distsim.crf.ser.gz',
			];
			$lang        = $this->lang->detect( $str )->bestResults()->close();
			if ( 1 === count( $lang ) ) {
				$lang = $lookupTable[ array_keys( $lang )[0] ];
			} else {
				$lang = $lookupTable['en'];
			}
			$regex = new \RegexIterator( $it, '/^.+\.crf\.ser\.gz/i', \RecursiveRegexIterator::GET_MATCH );
			foreach ( $regex as $value ) {
				if ( stripos( $value[0], $lang ) !== false ) {
					$this->setClassifier( $value[0] );
					break;
				}
			}
			if ( empty( $this->getClassifier() ) ) {
				throw new \RuntimeException( 'No classifier was found!' );
			}
		}
		if ( empty( $this->getJarArchive() ) ) {
			$regex = new \RegexIterator( $it, '/^.+stanford-ner\.jar$/i', \RecursiveRegexIterator::GET_MATCH );
			foreach ( $regex as $value ) {
				$this->setJarArchive( $value[0] );
				break;
			}
			if ( empty( $this->getJarArchive() ) ) {
				throw new \RuntimeException( 'Could not found any .jar files!' );
			}
		}
		// java -cp stanford-ner/stanford-ner.jar edu.stanford.nlp.process.PTBTokenizer train20.txt > train20.tok
		$cmd            = escapeshellcmd(
			$this->getJavaPath()
			. ' -mx' . $this->getMaxMemoryUsage()
			. ' -cp "' . $this->getJarArchive() . PATH_SEPARATOR . '" edu.stanford.nlp.process.PTBTokenizer'
		);
		$cmd            .= ' ' . escapeshellcmd( $path_file_txt ) . ' > ' . escapeshellcmd( $path_file_tox );
		$descriptorspec = [
			0 => [ "pipe", "r" ],
			1 => [ "pipe", "w" ],
			2 => [ "pipe", "r" ]
		];
		$process        = proc_open( $cmd, $descriptorspec, $pipes );
		$output         = null;
		if ( is_resource( $process ) ) {
			fclose( $pipes[0] );
			$output = stream_get_contents( $pipes[1] );
			fclose( $pipes[1] );
			fclose( $pipes[2] );
			proc_close( $process );
		}
		$outjavatok = trim( $output );
		// perl -ne 'chomp; print "$_\tO\n"' train20.tok > train20.tsv
//		$cmd            = escapeshellcmd(
//			$this->getPerlPath()
//		);
//		$cmd            .= ' -ne \'chomp; print "$_\tO\n"\' ' . escapeshellcmd( $path_file_tox ) . ' > ' . escapeshellcmd( $path_file_tsv );
//		$descriptorspec = [
//			0 => [ "pipe", "r" ],
//			1 => [ "pipe", "w" ],
//			2 => [ "pipe", "r" ]
//		];
//		$process        = proc_open( $cmd, $descriptorspec, $pipes );
//		$output         = null;
//		if ( is_resource( $process ) ) {
//			fclose( $pipes[0] );
//			$output = stream_get_contents( $pipes[1] );
//			fclose( $pipes[1] );
//			fclose( $pipes[2] );
//			proc_close( $process );
//		}
//		$outperltsv = trim( $output );
		$learn      = array();
		$fp         = fopen( realpath( $path_file_tox ), 'r' );
		$row        = 0;
		while ( ( $line = fgetcsv( $fp, 0, "\t" ) ) !== false ) {
			if ( $line ) {
				$learn[ $line[0] ] = 'O';
				$row ++;
			}
		}
		$arr     = array_replace_recursive( array_change_key_case( $learn, CASE_LOWER ), $kamus );
		$content = '';
		foreach ( $arr as $key => $val ) {
			$content .= "$key\t$val\n";
		}
		file_put_contents( $path_file_tsv, $content );
		// java -cp stanford-ner/stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -trainFile train20.tsv -serializeTo ner-model.ser.gz -prop train.prop
		$cmd            = escapeshellcmd(
			$this->getJavaPath()
			. ' -mx' . $this->getMaxMemoryUsage()
			. ' -cp "' . $this->getJarArchive() . PATH_SEPARATOR . '" edu.stanford.nlp.ie.crf.CRFClassifier'
			. ' -trainFile "' . $path_file_tsv
			. '" -serializeTo "' . $this->getClassifier()
			. '" -prop "' . $this->getProp() . '"'
		);
		$descriptorspec = [
			0 => [ "pipe", "r" ],
			1 => [ "pipe", "w" ],
			2 => [ "pipe", "r" ]
		];
		$process        = proc_open( $cmd, $descriptorspec, $pipes );
		$output         = null;
		if ( is_resource( $process ) ) {
			fclose( $pipes[0] );
			$output = stream_get_contents( $pipes[1] );
			fclose( $pipes[1] );
			fclose( $pipes[2] );
			proc_close( $process );
		}
		$outjavalearn = trim( $output );
		return $outjavatok . "\n" . "\n" . $outjavalearn;
	}
	public function setClassifier( string $classifier ) {
		if ( ! file_exists( $classifier ) ) {
			throw new \InvalidArgumentException( 'No classifier was found!' );
		}
		$this->classfier = $classifier;
	}
	public function getClassifier() {
		return $this->classfier;
	}
}