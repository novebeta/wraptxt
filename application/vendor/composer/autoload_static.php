<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbaa76c03eddfc97fbf95101d396fd9c0
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'StanfordTagger\\' => 15,
        ),
        'L' => 
        array (
            'LanguageDetection\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'StanfordTagger\\' => 
        array (
            0 => __DIR__ . '/..' . '/patrickschur/stanford-nlp-tagger/src/StanfordTagger',
        ),
        'LanguageDetection\\' => 
        array (
            0 => __DIR__ . '/..' . '/patrickschur/language-detection/src/LanguageDetection',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbaa76c03eddfc97fbf95101d396fd9c0::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbaa76c03eddfc97fbf95101d396fd9c0::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
